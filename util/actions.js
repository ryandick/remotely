//	forever-remote
var fremote = require('../lib/forever-remote/');
var actions = {
	//	list all procs
	list: function(host, port, callback) {
		fremote.createClient(port, host, function(err, forever) {
			if (err) callback(err, null);
			else {
				forever.list(null, function(err, procs) {
					if (err) callback(err, null);
					else {
						callback(null, procs);
						forever.end();
					}
				});
			}
		});
	}
};

module.exports = actions;