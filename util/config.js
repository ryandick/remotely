//	depends
var fs = require('fs');
var config = {
	read: function(filename) {
		if (filename) {
			var returnConf = fs.readFileSync(filename);
			//	return object
			return JSON.parse(returnConf);
		} else {
			//	config file path not supplied
			return false;
		}
	}
};
module.exports = config;