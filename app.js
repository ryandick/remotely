/**
 * Module dependencies.
 */

var express = require('express'),
  routes = require('./routes'),
  sec = require('./routes/sec'),
  http = require('http'),
  config = require('./util/config'),
  actions = require('./routes/actions'),
  fremote = require('./lib/forever-remote/'),
  path = require('path');

//  establish express object
var app = express();

//  extend config to contain configuration information
config.server = config.read('./conf/server.json');
//  start forever monitor service
fremote.createServer(config.server.forever.port);
console.log('Proc manager running on port ' + config.server.forever.port);
//  configure express
app.configure(function() {
  app.set('port', config.server.http.port || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'hjs');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser('3ncrYpti0n'));
  app.use(express.session());
  app.use(app.router);
  app.use(require('less-middleware')({
    src: __dirname + '/' + config.server.http.rootdir,
    compress: true,
    optimization: 2
  }));
  app.use(express.static(path.join(__dirname, config.server.http.rootdir)));
});

app.configure('development', function() {
  app.use(express.errorHandler());
});

//  routes
//  landing
app.get('/', routes.index);
//  secure
app.get('/sec', sec.status);
app.get('/sec/services', sec.services);
app.get('/sec/commands', sec.commands);
//  forever actions, json only
app.post('/sec/actions/list', actions.list);


http.createServer(app).listen(app.get('port'), function() {
  console.log("Express server listening on port " + app.get('port'));
});