/*
 * Actions relating to forever
 */

//	deps
var actions = require('../util/actions'),
	config = require('../util/config');
//	parse config file
config.server = config.read('./conf/server.json');

//	list all forever procs and return representation in object using actions
exports.list = function(req, res) {
	actions.list(config.server.forever.host, config.server.forever.port, function(err, procList) {
		if (err) res.json({
			hasErr: true,
			errMsg: err,
			result: null
		});
		else res.json({
			result: procList
		});
	});
};