/*
 * Secure Section Of Site
 */

//	secure status page
exports.status = function(req, res) {
	res.render('status', {
		title: 'Remotely',
		username: req.session.username
	});
};

//	control long running node services
exports.services = function(req, res) {
	res.render('services', {
		title: 'Remotely',
		username: req.session.username
	});
};

//	execute commands
exports.commands = function(req, res) {
	res.render('commands', {
		title: 'Remotely',
		username: req.session.username
	});
};