CHECK=✔
HR=\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#

PUBLIC_DIR = ./public
OUT_DIR = ./out

BOOTSTRAP_DIR = ./assets/bootstrap
BOOTSTRAP_LESS = ${BOOTSTRAP_DIR}/less/bootstrap_font-awesome.less
BOOTSTRAP_RESPONSIVE_LESS = ${BOOTSTRAP_DIR}/less/responsive.less

DARKSTRAP_DIR = ./assets/darkstrap

FONTAWESOME_DIR = ./assets/font-awesome

JQUERY_DIR = ./assets/jquery

MUSTACHE_DIR = ./assets/mustache

build:_build_header _build_prep_dirs _build_fontawesome _build_bootstrap _build_darkstrap_css _build_clientside_js _build_css _build_jquery _build_templates _build_mustache _build_footer

_build_header:
	@echo "${HR}"

_build_prep_dirs:
	@rm -fr ${OUT_DIR}
	@mkdir -p ${OUT_DIR}
	@mkdir -p ${OUT_DIR}/templates
	@mkdir -p ${OUT_DIR}/templates/services
	@mkdir -p ${OUT_DIR}/font
	@mkdir -p ${OUT_DIR}/css
	@mkdir -p ${OUT_DIR}/js
	@mkdir -p ${OUT_DIR}/js/thirdparty
	@mkdir -p ${OUT_DIR}/img
	@echo "$(shell date +%I:%M:%S%p) - Clean Dirs      	${CHECK} Done"

_build_clientside_js:
	@cat ${PUBLIC_DIR}/js/*.js > ${OUT_DIR}/js/clientside.js
	@echo "$(shell date +%I:%M:%S%p) - Javascript 	${CHECK} Done"

_build_css:
	@cat ${PUBLIC_DIR}/css/*.css > ${OUT_DIR}/css/style.css
	@echo "$(shell date +%I:%M:%S%p) - Styling       	${CHECK} Done"

_build_fontawesome:
	@sed 's/\..\/font/\/font/' ${FONTAWESOME_DIR}/less/font-awesome.less > ${BOOTSTRAP_DIR}/less/font-awesome.less
	@sed 's/sprites.less/font-awesome.less/' ${BOOTSTRAP_DIR}/less/bootstrap.less > ${BOOTSTRAP_LESS}
	@cp ${FONTAWESOME_DIR}/font/* ${OUT_DIR}/font/
	@echo "$(shell date +%I:%M:%S%p) - FontAwesome       	${CHECK} Done"

_build_bootstrap:
	@recess --compile --compress ${BOOTSTRAP_LESS} > ${OUT_DIR}/css/bootstrap.css
	@recess --compile --compress ${BOOTSTRAP_RESPONSIVE_LESS} > ${OUT_DIR}/css/bootstrap-responsive.css
	@cp ${BOOTSTRAP_DIR}/img/* ${OUT_DIR}/img/
	@cat ${BOOTSTRAP_DIR}/js/bootstrap-transition.js ${BOOTSTRAP_DIR}/js/bootstrap-alert.js ${BOOTSTRAP_DIR}/js/bootstrap-button.js ${BOOTSTRAP_DIR}/js/bootstrap-carousel.js ${BOOTSTRAP_DIR}/js/bootstrap-collapse.js ${BOOTSTRAP_DIR}/js/bootstrap-dropdown.js ${BOOTSTRAP_DIR}/js/bootstrap-modal.js ${BOOTSTRAP_DIR}/js/bootstrap-tooltip.js ${BOOTSTRAP_DIR}/js/bootstrap-popover.js ${BOOTSTRAP_DIR}/js/bootstrap-scrollspy.js ${BOOTSTRAP_DIR}/js/bootstrap-tab.js ${BOOTSTRAP_DIR}/js/bootstrap-typeahead.js ${BOOTSTRAP_DIR}/js/bootstrap-affix.js > ${OUT_DIR}/js/thirdparty/bootstrap.js
	@uglifyjs -nc ${OUT_DIR}/js/thirdparty/bootstrap.js > ${OUT_DIR}/js/thirdparty/bootstrap.min.js
	@rm ${OUT_DIR}/js/thirdparty/bootstrap.js
	@rm ${BOOTSTRAP_DIR}/less/font-awesome.less
	@rm ${BOOTSTRAP_LESS}
	@echo "$(shell date +%I:%M:%S%p) - Bootstrap       	${CHECK} Done"

_build_darkstrap_css:
	@cp ${DARKSTRAP_DIR}/stylesheets/darkstrap.css ${OUT_DIR}/css/
	@echo "$(shell date +%I:%M:%S%p) - Darkstrap       	${CHECK} Done"

_build_jquery:
	@grunt --base ${JQUERY_DIR}/ --config  ${JQUERY_DIR}/grunt.js  > /dev/null 2> /dev/null
	@cp ${JQUERY_DIR}/dist/jquery.min.js ${OUT_DIR}/js/thirdparty/
	@echo "$(shell date +%I:%M:%S%p) - jQuery       	${CHECK} Done"

_build_templates:
	@cp ${PUBLIC_DIR}/templates/services/*.mu.html ${OUT_DIR}/templates/services/
	@echo "$(shell date +%I:%M:%S%p) - Templates       	${CHECK} Done"

_build_mustache:
	@uglifyjs -nc ${MUSTACHE_DIR}/mustache.js > ${OUT_DIR}/js/thirdparty/mustache.min.js
	@cat ${MUSTACHE_DIR}/jquery.mustache.js > ${OUT_DIR}/js/thirdparty/jquery.mustache.min.js
	@echo "$(shell date +%I:%M:%S%p) - Mustache       	${CHECK} Done"

_build_footer:
	@echo "${HR}"