//	main clientside object
var page = {
	auth: {
		uid: null
	},
	bootstraps: {
		services: {
			viewall: function() {
				$(document).on('click', '.action_viewall', function(e) {
					//	get data
					page.post.services.viewall();
					//	get  template
					page.templates.services.viewall.get();
					//	toggle active
					page.utils.toggleActive.services.viewall('action');
					//	render template if data and template are present
					$('.groupcontent').html(page.render(page.data.services.viewall, page.templates.services.viewall.content));
				});
			},
			init: function() {
				//	main init for services
				page.bootstraps.services.viewall();
			},
			destroy: function() {
				page.destroy.services.viewall();
			}
		}
	},
	data: {
		services: {
			viewall: null
		}
	},
	templates: {
		services: {
			viewall: {
				content: null,
				//	get template
				get: function() {
					//	if not cached
					if (!page.templates.services.viewall.content) {
						$.ajax({
							url: '/templates/services/viewall.mu.html',
							type: 'get',
							success: function(data) {
								if (data) {
									page.templates.services.viewall.content = data;
									return data;
								} else {
									return page.templates.services.viewall.content;
								}
							}
						});
					} else {
						return page.templates.services.viewall.content;
					}
				}
			}
		}
	},
	post: {
		services: {
			viewall: function() {
				jQuery.ajax({
					url: '/sec/actions/list',
					type: 'post',
					data: {
						uid: page.auth.uid
					},
					success: function(data) {
						page.data.services.viewall = data;
						return data;
					}
				});
			}
		}
	},
	get: {

	},
	utils: {
		toggleActive: {
			services: {
				viewall: function(classname) {
					$('.actions').removeClass('active');
					$('.' + classname).addClass('active');
				}
			}
		}
	},
	destroy: {
		services: {
			viewall: function() {
				$(document).off('click', '.action_viewall');
			}
		}
	},
	render: function(data, template) {
		return Mustache.to_html(template,data,null);
	},
	error:function(errorMsg){
		if(errorMsg){
			$('.errMsg').html(errorMsg);
			$('.stderr').fadeIn('slow');
			return true;
		}else{
			return false;
		}
	}
};